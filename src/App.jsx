import Intro from './component/intro/intro'
import About from './component/about/about';
import ProductList from './component/productList/productList';
import Contact from './component/contact/Contact';
import Part from './component/partenaire/part';
function App() {
  return (
    <div className="App">
      <Intro/>
      <About/>
      <ProductList/>
      <Part/>
      <Contact/>
      
    </div>
  );
}

export default App;
