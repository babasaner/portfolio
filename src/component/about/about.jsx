import React from 'react'
import Award from "../../img/award.png"
import './about.css'
import Baba from "../../img/babasaner.jpg"
const about = () => {
  return (
    <div className="a">

        <div className="a-left">
            <div className="a-card bg"></div>
            <div className="a-card">
                <img src={Baba} alt="Baba sané" className="a-img" />
            </div>
        </div>

        <div className="a-right">
        <h1 className="a-title">A Propos</h1>
        <p className="a-desc">
        Je suis Elhadji Mamadou Lamine Sané Alias Baba Sané, un développeur front-end passionné par la création d'expériences web captivantes et interactives. J'ai dédié mon parcours professionnel à combiner mon amour pour le design et ma maîtrise des technologies web pour donner vie à des interfaces utilisateur exceptionnelles. Aujourd'hui, je suis ravi de partager avec vous mon parcours et mes compétences en tant que développeur front-end.
        </p>
    
        </div>
    </div>
  )
}

export default about