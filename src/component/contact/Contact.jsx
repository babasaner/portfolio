import React, { useRef, useState } from 'react'
import './contact.css'
import Phone from "../../img/phone.png"
import Email from "../../img/email.png"
import Adress from "../../img/address.png"
import emailjs from '@emailjs/browser';


const Contact = () => {
    const formRef =useRef()
    const [done, setDone] = useState(false)

    const handleSubmit = (e) =>{
        e.preventDefault();


        emailjs.sendForm('service_i4769tz', 'template_pvr59r2', formRef.current, 'mnI5i4RpodLkskRJM')
        .then((result) => {
            console.log(result.text);
            setDone(true)
        }, (error) => {
            console.log(error.text);
        });
    


    };
  return (
    <div className='c'>
        <div className="c-bg"></div>
        <div className="c-wrapper">
            <div className="c-left">
                <h1 className="c-title">Parler nous de votre projet</h1>
                <div className="c-info">
                    <div className="c-info-item">
                        <img src={Phone} alt="" className="c-icon" />
                        +221 77 456 27 27
                    </div>

                    <div className="c-info-item">
                        <img src={Adress} alt="" className="c-icon" />
                        Cité Sipres, Zac Mbao en face Auchan
                    </div>

                    <div className="c-info-item">
                        <img src={Email} alt="" className="c-icon" />
                        contact@solutechinnov.com
                    </div>
                </div>
            </div>
            <div className="c-right">
                <p className="c-desc">
                 Prenez contact avec nous. Nous sommes disponible pour
            discuter de votre projet.
                </p>
                <form ref={formRef} onSubmit={handleSubmit}>
                    <input type="text" placeholder="Prénom & Nom" name="nom" />
                    <input type="email" placeholder="Email" name="email" />
                    <input type="text" placeholder="Sujet" name="sujet" />
                    <textarea name="message" cols="30" placeholder="Message" rows="5"></textarea>
                    <button>Envoyer</button>
                    {done && "Votre Message a été envoyé avec succées"}
                </form>
            </div>
        </div>
    </div>
  )
}

export default Contact