import React from 'react'
import './part.css'
import Sov from './partenaires/sovereign-studio-logo.png'
import Vizir from './partenaires/VizirGroup.png'
import Bic from './partenaires/LOGO_BIC.png'
import Maryshop from './partenaires/maryshop_logo.png'
import Soloway from './partenaires/soloway.svg'
import Eden from './partenaires/logo-enden-estate.svg'

const part = () => {
  return (
    <div className="part">
        <h1>Ils nous ont fait confiance</h1>
        <div className="logo">
          <div>
            <img src={Sov} alt="Sovereign-studio"  />
            <img src={Vizir} alt="Vizir Group"  />
            <img src={Bic} alt="BIC-EQUIPEMENT"  />
            <img src={Maryshop} alt="Maryshop By Maité"/>
            <img src={Soloway} alt="Soloway"/>
            <img src={Eden} alt="Eden Estates" />
          </div>
        </div>
        
    </div>
  )
}

export default part