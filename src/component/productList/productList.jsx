import React from 'react'
import './productList.css'
import Product from "../product/product"
import { products } from '../../projet'

const productList = () => {
  return (
    <div className='pl'>
      <div className="pl-texts">
      <h1 className="pl-title">Portfolio</h1>
      
      </div>
      <div className="pl-list">
        {products.map((item) =>(<Product key={item.id} img={item.img} link={item.link}/>
         
        ))}
        
        
      </div>
    </div>
  )
}

export default productList