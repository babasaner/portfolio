import Mermozine from './img/residencemermozine.jpg';
import Akwa from './img/akwaresidence.jpg'
import Allure from './img/residenceallure.jpg'
import Majestueux from "./img/residencelemajestueux.jpg"
import Palmeraie from "./img/residencelapalmeraie.jpg"
import Signature from "./img/signaturebya1.jpg"
import Vizir from "./img/vizirgroup.jpg"
import Soloway from "./img/SOLOWAY.jpg"
import Siki from './img/Siki.png'
import Sovereign from './img/Sovereign.png'
import Eden from './img/Eden.png'
import Dgpu from './img/dgpu.png'
import Maite from './img/Maryshop.png'
export const products = [
    {
      id: 1,
      img: Mermozine,
      link: "http://residencemermozine.com",
    },
    {
      id: 2,
      img: Akwa,
      link: "http://akwaresidence.com",
    },
    {
      id: 3,
      img: Allure,
      link: "http://residenceallure.com",
    },
    {
      id: 4,
      img: Majestueux,
      link: "https://residencelemajestueux.com"
    },
    {
      id: 5,
      img: Palmeraie,
      link: "https://www.residencelapalmeraie.com",
    },
    {
      id: 6,
      img: Signature,
      link: "https://signaturebya1.com/",
    },

    {
        id: 7,
        img: Soloway,
        link: "https://www.soloway.group",
      },

      {
        id: 8,
        img: Siki,
        link: "https://www.residence-siki.sn",
      },

      {
        id: 9,
        img: Vizir,
        link: "https://www.vizirgroup.com",
      },
      {
        id: 10,
        img: Sovereign,
        link: "https://www.sovereign-studio.com",
      },

      {
        id: 11,
        img: Eden,
        link: "https://www.eden-estates.com",
      },

      {
        id: 12,
        img: Dgpu,
        link: "https://www.dgpu.org",
      },
      {
        id: 13,
        img: Maite,
        link: "https://www.maryshop.store",
      },
  ];